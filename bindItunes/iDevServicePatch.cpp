// IDevServicePatch.cpp : main project file.

#include "stdafx.h"
#include <shlwapi.h>
#include <cfgmgr32.h>

#pragma comment(lib, "setupapi.lib")
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "cfgmgr32.lib")

using namespace System;

TCHAR sIntanceId[MAX_PATH] = { 0 };

GUID guid_apple_mux_device = { 0xf0b32be3, 0x6678, 0x4879, { 0x92, 0x30, 0xe4, 0x38, 0x45, 0xd8, 0x05, 0xee } };




int GetFirstIdeviceInstance()
{
	int Ret = ERROR_SUCCESS;
	logIt("GetFirstIdeviceInstance ++ ");
	HDEVINFO hDevInfo = SetupDiGetClassDevs(&guid_apple_mux_device, NULL, NULL, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
	if (INVALID_HANDLE_VALUE != hDevInfo)
	{
		SP_DEVINFO_DATA DeviceInfoData;
		ZeroMemory(&DeviceInfoData, sizeof(SP_DEVINFO_DATA));
		DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		int DeviceIndex = 0;
		while (SetupDiEnumDeviceInfo(hDevInfo, DeviceIndex, &DeviceInfoData))
		{
			DeviceIndex++;
			DEVPROPTYPE PropType;
			DWORD Size = 0;

			ZeroMemory(sIntanceId, sizeof(sIntanceId));

			if (!SetupDiGetDeviceProperty(hDevInfo, &DeviceInfoData,
				&DEVPKEY_Device_InstanceId,
				&PropType,
				(PBYTE)&sIntanceId,
				sizeof(sIntanceId) * sizeof(TCHAR),
				&Size,
				0) || PropType != DEVPROP_TYPE_STRING) {

				Ret = GetLastError();
				logIt(String::Format("GetFirstIdeviceInstance: ret = {0}", Ret));
			}
			else
			{
				DEVINST parent;
				if (CM_Get_Parent(&parent, DeviceInfoData.DevInst, 0) == CR_SUCCESS) {
					TCHAR sparentIntanceId[MAX_PATH] = { 0 };
					if (CM_Get_Device_ID(parent, sparentIntanceId, MAX_PATH, 0) == CR_SUCCESS)
					{
						logIt(String::Format("Parent ID: {0}", gcnew String(sparentIntanceId)));
						//USB\VID_05AC&PID_12A8\9F6C3777D4B0209039F4B0F460E7F141E793B7D1
						if (_tcsncmp(sparentIntanceId, _T("USB\\VID_05AC&"), 12) == 0) {
							//_tprintf(_T("copy to task ID: %s\n"), sparentIntanceId);
							logIt(String::Format("copy to task ID: {0}", gcnew String(sparentIntanceId)));
							ZeroMemory(sIntanceId, sizeof(sIntanceId));
							_tcscpy_s(sIntanceId, sparentIntanceId);
						}
					}
				}
				if (_tcslen(sIntanceId) > 0)
				{
					Ret = ERROR_SUCCESS;
					_tprintf(sIntanceId);
					logIt(String::Format("InstanceID: {0}", gcnew String(sIntanceId)));
					break;
				}
			}
		}
		if (hDevInfo) {
			SetupDiDestroyDeviceInfoList(hDevInfo);
		}
	}
	return Ret;
}

int runExe(LPCTSTR exe, LPCTSTR args, int timeout)
{
	int ret = NO_ERROR;
	_tprintf(_T("runExe: ++ exe=%s, arg=%s\n"), exe, args);
	logIt(String::Format("runExe: ++ exe={0}, arg={1}", gcnew String(exe), gcnew String(args)));
	if (PathFileExists(exe))
	{
		DWORD exitCode;
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		ZeroMemory(&si, sizeof(STARTUPINFO));
		si.cb = sizeof(STARTUPINFO);
		si.wShowWindow = SW_HIDE;
		TCHAR temp[1024] = { 0 };
		_stprintf_s(temp, _T("\"%s\" %s"), exe, args);
		if (CreateProcess(NULL, temp, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
		{

			if (WaitForSingleObject(pi.hProcess, timeout * 1000) == WAIT_OBJECT_0)						// timeout 20 mins
			{
				GetExitCodeProcess(pi.hProcess, &exitCode);
				ret = exitCode;
			}
			else
			{
				ret = ERROR_TIMEOUT;
				// kill all process tree
				TerminateProcess(pi.hProcess, 100);
			}
			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
		}
		else
			ret = ERROR_CREATE_FAILED;
	}
	else
		ret = ERROR_FILE_NOT_FOUND;
	_tprintf(_T("runExe: -- return %d\n"), ret);
	logIt(String::Format("runExe: -- return: {0}",ret));
	return ret;
}

int RestartUSBDevice()
{
	TCHAR exe[MAX_PATH] = { 0 };
	int ret = ERROR_NOT_FOUND;
	//GetModuleFileName(NULL, exe, MAX_PATH);
	GetEnvironmentVariable(_T("APSTHOME"), exe, MAX_PATH);
	//PathRemoveFileSpec(exe);
	if (!Environment::Is64BitOperatingSystem)
	{
		PathAppend(exe, _T("tools\\devcon.exe"));
	}
	else {
		PathAppend(exe, _T("tools\\devcon64.exe"));
	}
	//_tprintf(exe);
	if (PathFileExists(exe) && _tcslen(sIntanceId) > 0)
	{
		TCHAR args[MAX_PATH] = { 0 };
		_stprintf_s(args, _T("restart @\"%s\""), sIntanceId);
		_tprintf(_T("will run devcon: %s --- %s\n"), exe, args);
		ret = runExe(exe, args, 30);
	}
	else
		_tprintf(_T("devcon is not exist or not instanceID! %s\n"), sIntanceId);

	return ret;
}

int ApplePrepareDriver() 
{
	TCHAR exe[MAX_PATH] = { 0 };
	int ret = ERROR_NOT_FOUND;
	//GetModuleFileName(NULL, exe, MAX_PATH);
	GetEnvironmentVariable(_T("APSTHOME"), exe, MAX_PATH);
	PathAppend(exe, _T("AppleHelper.exe"));
	if (PathFileExists(exe))
	{
		TCHAR args[MAX_PATH] = { 0 };
		_stprintf_s(args, _T("-label=0 -func=preparedriver"));
		_tprintf(_T("AppleHelper: %s --- %s\n"), exe, args);
		ret = runExe(exe, args, 30);
	}
	return ret;
}

int DEDriverUSBDevice(BOOL bDisable = TRUE)
{
	TCHAR exe[MAX_PATH] = { 0 };
	int ret = ERROR_NOT_FOUND;
	//GetModuleFileName(NULL, exe, MAX_PATH);
	GetEnvironmentVariable(_T("APSTHOME"), exe, MAX_PATH);
	//PathRemoveFileSpec(exe);
	if (!Environment::Is64BitOperatingSystem)
	{
		PathAppend(exe, _T("tools\\devcon.exe"));
	}
	else {
		PathAppend(exe, _T("tools\\devcon64.exe"));
	}
	//_tprintf(exe);
	if (PathFileExists(exe) && _tcslen(sIntanceId) > 0)
	{
		TCHAR args[MAX_PATH] = { 0 };
		if (bDisable) {
			_stprintf_s(args, _T("disable @\"%s\""), sIntanceId);
		}
		else {
			_stprintf_s(args, _T("enable @\"%s\""), sIntanceId);
		}
		_tprintf(_T("will run devcon: %s --- %s\n"), exe, args);
		ret = runExe(exe, args, 30);
	}
	else
		_tprintf(_T("devcon is not exist or not instanceID! %s\n"), sIntanceId);

	return ret;
}

