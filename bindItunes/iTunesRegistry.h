#pragma once

#using <mscorlib.dll>
#using <System.dll>
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Text;
using namespace System::Management;
using namespace System::Runtime::InteropServices;

namespace iTunesRegistry
{
	String^ RegAppleApplicationSupport();
	String^ RegAppleMobileDeviceSupport();
	String^ RegAppleMobileDeviceSupportShareDll(String^ sDllName);
	String^ RegAirTrafficHostDLL();
	String^ RegASMapiInterfaceDLL();
	String^ RegiTunesMobileDeviceDLL();
	String^ RegMobileDeviceDLL();
	bool IsInstalliTunes();
	void CreateiTunesRegInfo(String^ sBasePath);
}