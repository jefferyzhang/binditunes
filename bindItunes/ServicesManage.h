#pragma once

#using <mscorlib.dll>
#using <System.dll>
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Text;
using namespace System::Management;
using namespace System::Runtime::InteropServices;
using namespace System::ServiceProcess;

namespace Installation
{
	public enum class ServiceType :int
	{
		KernelDriver = 0x1,
		FileSystemDriver = 0x2,
		Adapter = 0x4,
		RecognizerDriver = 0x8,
		OwnProcess = 0x10,
		ShareProcess = 0x20,
		Interactive = 0x100
	};

	public enum class ReturnValue
	{
		Success = 0,
		NotSupported = 1,
		AccessDenied = 2,
		DependentServicesRunning = 3,
		InvalidServiceControl = 4,
		ServiceCannotAcceptControl = 5,
		ServiceNotActive = 6,
		ServiceRequestTimeout = 7,
		UnknownFailure = 8,
		PathNotFound = 9,
		ServiceAlreadyRunning = 10,
		ServiceDatabaseLocked = 11,
		ServiceDependencyDeleted = 12,
		ServiceDependencyFailure = 13,
		ServiceDisabled = 14,
		ServiceLogonFailure = 15,
		ServiceMarkedForDeletion = 16,
		ServiceNoThread = 17,
		StatusCircularDependency = 18,
		StatusDuplicateName = 19,
		StatusInvalidName = 20,
		StatusInvalidParameter = 21,
		StatusInvalidServiceAccount = 22,
		StatusServiceExists = 23,
		ServiceAlreadyPaused = 24,
		ServiceNotFound = 25
	};

	public enum class OnError
	{
		UserIsNotNotified = 0,
		UserIsNotified = 1,
		SystemRestartedLastGoodConfiguraion = 2,
		SystemAttemptStartWithGoodConfiguration = 3
	};

	public enum class StartMode
	{
		Boot = 0,
		System = 1,
		Automatic = 2,
		Manual = 3,
		Disabled = 4
	};

	public enum ServiceState
	{
		Running,
		Stopped,
		Paused,
		StartPending,
		StopPending,
		PausePending,
		ContinuePending
	};
	ReturnValue^ InstallService(String^ svcName, String^ svcDispName, String^ svcPath,
		ServiceType svcType, OnError errHandle, StartMode svcStartMode, bool interactWithDesktop,
		String^ svcStartName, String^ svcPassword, String^ loadOrderGroup,
		array<String^>^  loadOrderGroupDependencies, array<String^>^  svcDependencies);
	ReturnValue UninstallService(String^ svcName);
	ReturnValue StartServiceFD(String^ svcName);
	ReturnValue ChangeService(String^ svcName, String^ sfield, Object^ obj);
	ReturnValue StopService(String^ svcName);
	//ReturnValue ResumeService(String^ svcName);
	//ReturnValue PauseService(String^ svcName);
	//ReturnValue ChangeStartMode(String^ svcName, StartMode startMode);
	bool IsServiceInstalled(String^ svcName);
	ServiceControllerStatus GetServiceState(String^ svcName);
	bool CanStop(String^ svcName);
	bool CanPauseAndContinue(String^ svcName);
	int GetProcessId(String^ svcName);

	System::String^ GetPath(System::String^ svcName);
	bool ShowProperties(System::String^ svcName);
	System::String^ propData2String(System::Management::PropertyData^ pd);
}

