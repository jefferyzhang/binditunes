#include "stdafx.h"
#include "iTunesRegistry.h"
using namespace Microsoft::Win32;
using namespace System::IO;

namespace iTunesRegistry
{
	bool Isbit64OS()
	{
		return System::Environment::Is64BitOperatingSystem;
	}
	String^ RegAppleApplicationSupport()
	{
#ifdef _WIN64
		return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Application Support", "InstallDir", "")->ToString();
#else
		if (Isbit64OS())
		{
			return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Application Support", "InstallDir", "")->ToString();
		}
		else
		{
			return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Application Support", "InstallDir", "")->ToString();
		}
#endif
	}
	String^ RegAppleMobileDeviceSupport()
	{
#ifdef _WIN64
		return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Mobile Device Support", "InstallDir", "")->ToString();
#else
		if (Isbit64OS())
		{
			return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Mobile Device Support", "InstallDir", "")->ToString();
		}
		else
		{
			return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Mobile Device Support", "InstallDir", "")->ToString();
		}
#endif
	}
	String^ RegAppleMobileDeviceSupportShareDll(String^ sDllName)
	{
#ifdef _WIN64
		return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Mobile Device Support\\Shared", sDllName, "")->ToString();
#else
		if (Isbit64OS())
		{
			return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Mobile Device Support\\Shared", sDllName, "")->ToString();
		}
		else
		{
			return Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Mobile Device Support\\Shared", sDllName, "")->ToString();
		}
#endif
	}
	String^ RegAirTrafficHostDLL()
	{
		return RegAppleMobileDeviceSupportShareDll("AirTrafficHostDLL");
	}
	String^ RegASMapiInterfaceDLL()
	{
		return RegAppleMobileDeviceSupportShareDll("ASMapiInterfaceDLL");
	}
	String^ RegiTunesMobileDeviceDLL()
	{
		return RegAppleMobileDeviceSupportShareDll("iTunesMobileDeviceDLL");
	}
	String^ RegMobileDeviceDLL()
	{
		return RegAppleMobileDeviceSupportShareDll("MobileDeviceDLL");
	}
	
	bool IsInstalliTunes()
	{
		bool bRet = false;
		RegistryKey^ regit = Registry::LocalMachine;
		try
		{
			String^ strAMDS;
		//	String^ strAAS;
#ifdef _WIN64
			strAMDS = "SOFTWARE\\Apple Inc.";
#else
			if (Isbit64OS())
			{
				strAMDS = "SOFTWARE\\Wow6432Node\\Apple Inc.";//\\Apple Mobile Device Support
				//strAAS = "SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Application Support";
			}
			else
			{
				strAMDS = "SOFTWARE\\Apple Inc.";//\\Apple Mobile Device Support
				//strAAS = "SOFTWARE\\Apple Inc.\\Apple Application Support";
			}
#endif
			RegistryKey^ hardWare = regit->OpenSubKey(strAMDS, true);
			//RegistryKey^ example = regit->OpenSubKey(strAAS, true);

			array<String^>^ subkeyNames = hardWare->GetSubKeyNames();
			for each(String^ str in subkeyNames)
			{
				if (String::Compare(str, "Apple Mobile Device Support", true) == 0)
				{
					bRet = true;
				}
			}

		}
		catch(Exception^ ex)
		{
			logIt(ex->ToString());
			bRet = false;
		}
		regit->Close();
		return bRet;
	}

	void CreateCoreADI(String^ sBasePath)
	{
		logIt("CreateCoreADI ++ " + sBasePath);
		String^ strAASCoreADI;
		String^ strAASPath = Path::Combine(sBasePath, "Apple Application Support\\CoreADI.dll");
		if (!Directory::Exists(strAASPath)) return;
#ifdef _WIN64
		strAASCoreADI = "SOFTWARE\\Apple Inc.\\CoreADI";
#else

		if (Isbit64OS())
		{
			strAASCoreADI = "SOFTWARE\\Wow6432Node\\Apple Inc.\\CoreADI";
		}
		else
		{
			strAASCoreADI = "SOFTWARE\\Apple Inc.\\CoreADI";
		}
#endif
		RegistryKey^ regit = Registry::LocalMachine;
		RegistryKey^ keyAAS;
		try{
			keyAAS = regit->CreateSubKey(strAASCoreADI);
			keyAAS->SetValue("LibraryPath", strAASPath);
		}
		finally{
			keyAAS->Close();
		}

		logIt("CreateCoreADI -- ");
	}

	void CreateCoreFP(String^ sBasePath)
	{
		logIt("CreateCoreFP ++ " + sBasePath);
		String^ strAASCoreFP;
		String^ strAASPath = Path::Combine(sBasePath, "CoreFP\\CoreFP.dll");
		if (!Directory::Exists(strAASPath)) return;
#ifdef _WIN64
		strAASCoreFP = "SOFTWARE\\Apple Inc.\\CoreFP";
#else

		if (Isbit64OS())
		{
			strAASCoreFP = "SOFTWARE\\Wow6432Node\\Apple Inc.\\CoreFP";
		}
		else
		{
			strAASCoreFP = "SOFTWARE\\Apple Inc.\\CoreFP";
		}
#endif
		RegistryKey^ regit = Registry::LocalMachine;
		RegistryKey^ keyAAS;
		try{
			keyAAS = regit->CreateSubKey(strAASCoreFP);
			keyAAS->SetValue("LibraryPath", strAASPath);
		}
		finally{
			keyAAS->Close();
		}
#ifdef _WIN64
#else
		if (Isbit64OS())
		{
			strAASPath = Path::Combine(sBasePath, "CoreFP_x64\\CoreFP.dll");
			if (!Directory::Exists(strAASPath)) return;
			try{
				keyAAS = regit->CreateSubKey("SOFTWARE\\Apple Inc.\\CoreFP");
				keyAAS->SetValue("LibraryPath", strAASPath);
			}
			finally
			{
				keyAAS->Close();
			}
		}
#endif
		logIt("CreateCoreFP --");
	}

	void CreateiTunesRegInfo(String^ sBasePath)
	{
		logIt("CreateiTunesRegInfo ++ "+sBasePath);
		String^ strAMDS;
		//String^ strAAS;
		String^ strAMDSPath = Path::Combine(sBasePath, "Mobile Device Support\\");
		//String^ strAASPath = Path::Combine(sBasePath, "Apple Application Support\\");

		if (!Directory::Exists(strAMDSPath)/* || !Directory::Exists(strAASPath)*/) return;
#ifdef _WIN64
		strAMDS = "SOFTWARE\\Apple Inc.\\Apple Mobile Device Support";
#else
		if (Isbit64OS())
		{
			strAMDS = "SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Mobile Device Support";
			//strAAS = "SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Application Support";
		}
		else
		{
			strAMDS = "SOFTWARE\\Apple Inc.\\Apple Mobile Device Support";
			//strAAS = "SOFTWARE\\Apple Inc.\\Apple Application Support";
		}
#endif
		logIt("AMDS:" + strAMDS);
		//logIt("AAS:" + strAAS);
		RegistryKey^ regit = Registry::LocalMachine;
		RegistryKey^ keyAMDS;
//		RegistryKey^ keyAAS;
		try{
			/*if (IsInstalliTunes())
			{
				keyAMDS = regit->OpenSubKey(strAMDS, true);
				keyAAS = regit->OpenSubKey(strAAS, true);
				logIt("IsInstalliTunes");
			}
			else*/
			{
				keyAMDS = regit->CreateSubKey(strAMDS);
				//keyAAS = regit->CreateSubKey(strAAS);
			}
			//logIt("AASPATH:" + strAASPath);
			//keyAAS->SetValue("InstallDir", strAASPath);
			logIt("AASPATH:" + strAMDSPath);
			keyAMDS->SetValue("InstallDir", strAMDSPath);
			RegistryKey^ keyAMDSShared = keyAMDS->CreateSubKey("Shared");
			keyAMDSShared->SetValue("AirTrafficHostDLL",Path::Combine(strAMDSPath,"AirTrafficHost.DLL"));
			keyAMDSShared->SetValue("ASMapiInterfaceDLL", Path::Combine(strAMDSPath, "ASMapiInterface.DLL"));
			//keyAMDSShared->SetValue("iTunesMobileDeviceDLL", Path::Combine(strAMDSPath, "iTunesMobileDevice.DLL"));
			keyAMDSShared->SetValue("MobileDeviceDLL", Path::Combine(strAMDSPath, "MobileDevice.DLL"));
		}
		catch (Exception^ ex)
		{
			logIt(ex->ToString());
		}
		regit->Close();

		//CreateCoreFP(sBasePath);  //new CoreFP in itunes Folder
		//CreateCoreADI(sBasePath);
		logIt("CreateiTunesRegInfo --" );
	}
}