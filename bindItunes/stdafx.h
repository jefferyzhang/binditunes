// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#include <Windows.h>
#include <tchar.h>

#include <initguid.h>

#include <usbioctl.h>
#include <usbiodef.h>

#define INITGUID
#include <initguid.h>
#include <SetupAPI.h>
#include <Devpropdef.h>
#include <Devpkey.h>
// TODO: reference additional headers your program requires here
void logIt(System::String^ msg);