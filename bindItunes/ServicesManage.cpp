#include "stdafx.h"
#include  <stdio.h>
#include <cstdlib>
#include <winsvc.h>
#include < vcclr.h >
#include "ServicesManage.h"
#pragma comment(lib, "advapi32.lib")

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Text;
using namespace Microsoft::Win32;
using namespace System::Management;
using namespace System::Runtime::InteropServices;
using namespace System::ServiceProcess;

namespace Installation
{
	ReturnValue^ InstallService(String^ svcName, String^ svcDispName, String^ svcPath, 
		ServiceType svcType, OnError errHandle,	StartMode svcStartMode, bool interactWithDesktop, 
		String^ svcStartName, String^ svcPassword, String^ loadOrderGroup, 
		array<String^>^  loadOrderGroupDependencies, array<String^>^  svcDependencies)
	{
		logIt(String::Format("InstallService  {0}", svcName));
		SC_HANDLE schSCManager;
		SC_HANDLE schService;
		schSCManager = OpenSCManager(
			NULL,                    // local computer
			NULL,                    // ServicesActive database 
			SC_MANAGER_ALL_ACCESS);  // full access rights 
		if (nullptr == schSCManager)
		{
			logIt(String::Format("OpenSCManager failed {0}", GetLastError()));
			return ReturnValue::UnknownFailure;
		}
		pin_ptr<const wchar_t> svname = PtrToStringChars(svcName);
		pin_ptr<const wchar_t> svdisplayname = PtrToStringChars(svcDispName);
		pin_ptr<const wchar_t> svpath = PtrToStringChars(svcPath);
		schService = CreateService(
			schSCManager,              // SCM database 
			svname,                   // name of service 
			svdisplayname,                   // service name to display 
			SERVICE_ALL_ACCESS,        // desired access 
			SERVICE_WIN32_OWN_PROCESS, // service type 
			SERVICE_DEMAND_START,      // start type 
			SERVICE_ERROR_NORMAL,      // error control type 
			svpath,                    // path to service's binary 
			NULL,                      // no load ordering group 
			NULL,                      // no tag identifier 
			NULL,                      // no dependencies 
			NULL,                      // LocalSystem account 
			NULL);                     // no password 

		if (schService == NULL)
		{
			logIt(String::Format("CreateService failed {0}", GetLastError()));
			CloseServiceHandle(schSCManager);
			return ReturnValue::UnknownFailure;;
		}
		else logIt("Service installed successfully");

		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return ReturnValue::Success;


		/*ManagementClass^ mc = gcnew ManagementClass("Win32_Service");
		ManagementBaseObject^ inParams = mc->GetMethodParameters("create");
		try
		{
			inParams["Name"] = svcName; logIt("Name");
			inParams["DisplayName"] = svcDispName; logIt("DisplayName");
			inParams["PathName"] = svcPath; logIt("PathName");
			inParams["ServiceType"] = svcType; logIt("ServiceType");
			inParams["ErrorControl"] = errHandle; logIt("ErrorControl");
			inParams["StartMode"] = svcStartMode.ToString(); logIt("StartMode");
			inParams["DesktopInteract"] = interactWithDesktop; logIt("DesktopInteract");
			inParams["StartName"] = svcStartName; logIt("StartName");
			inParams["StartPassword"] = svcPassword; logIt("StartPassword");
			inParams["LoadOrderGroup"] = loadOrderGroup; logIt("LoadOrderGroup");
			inParams["LoadOrderGroupDependencies"] = loadOrderGroupDependencies; logIt("LoadOrderGroupDependencies");
			inParams["ServiceDependencies"] = svcDependencies; logIt("ServiceDependencies");
			inParams["Description"] = "Provides the interface to Apple mobile devices."; logIt("Description");
		} catch (...)
		{ }

		try
		{
			ManagementBaseObject^ outParams = mc->InvokeMethod("create", inParams, nullptr);

			return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
		}
		catch (...)
		{
			return ReturnValue::UnknownFailure;
		}*/
	}

	ReturnValue UninstallService(String^ svcName)
	{
		logIt(String::Format("UninstallService  {0}",svcName));

		pin_ptr<const wchar_t> svname = PtrToStringChars(svcName);
		// Open the Service Control Manager
		SC_HANDLE scmHandle = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if (scmHandle == NULL) {
			logIt(String::Format("OpenSCManager failed: {0}", GetLastError()));
			return ReturnValue::NotSupported;
		}

		// Open the service
		SC_HANDLE serviceHandle = OpenService(scmHandle, svname, DELETE);
		if (serviceHandle == NULL) {
			logIt(String::Format("OpenService failed: {0}", GetLastError()));
			CloseServiceHandle(scmHandle);
			return ReturnValue::StatusInvalidName;
		}

		// Delete the service
		if (!DeleteService(serviceHandle)) {
			logIt(String::Format("DeleteService failed: {0}", GetLastError()));
			return ReturnValue::AccessDenied;
		}
		
		// Close handles
		CloseServiceHandle(serviceHandle);
		CloseServiceHandle(scmHandle);
		return ReturnValue::Success;
		/*String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		try
		{
			ManagementBaseObject^ outParams = service->InvokeMethod("delete", nullptr, nullptr);

			return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
		}
		catch (...)
		{
			return ReturnValue::ServiceNotFound;
		}*/

	}

	ReturnValue StartServiceFD(String^ svcName)
	{
		logIt(String::Format("StartServiceFD  {0}", svcName));
		try {
			ServiceController^ sc = gcnew ServiceController(svcName);
			if (sc->Status == ServiceControllerStatus::Stopped)
			{
				sc->Start();
				sc->WaitForStatus(ServiceControllerStatus::Running);
				return ReturnValue::Success;
			}
		}catch(...){}
		return ReturnValue::AccessDenied;

		//String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		//ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		//try
		//{
		//	ManagementBaseObject^ outParams = service->InvokeMethod("StartService", nullptr, nullptr);

		//	return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
		//}
		//catch (...)
		//{
		//	//if (ex.Message.ToLower().Trim() == "not found" || ex.GetHashCode() == 41149443)
		//		return ReturnValue::ServiceNotFound;
		//}
	}

	ReturnValue ChangeService(String^ svcName, String^ sfield, Object^ obj)
	{
		logIt(String::Format("ChangeService  {0}", svcName));
		SC_HANDLE schSCManager;
		schSCManager = OpenSCManager(
			NULL,                    // local computer
			NULL,                    // ServicesActive database 
			SC_MANAGER_ALL_ACCESS);  // full access rights 
		if (nullptr == schSCManager)
		{
			logIt(String::Format("OpenSCManager failed {0}", GetLastError()));
			goto TRYREG;
			return ReturnValue::UnknownFailure;
		}
		pin_ptr<const wchar_t> svname = PtrToStringChars(svcName);
		pin_ptr<const wchar_t> newPath = PtrToStringChars(obj->ToString());
		//pin_ptr<const wchar_t> svpath = PtrToStringChars(svcPath);
		SC_HANDLE schService = OpenService(schSCManager, svname, SERVICE_ALL_ACCESS);

		if (schService == NULL) {
			logIt(String::Format("OpenService failed: {0}", GetLastError()));
			CloseServiceHandle(schSCManager);
			goto TRYREG;
			return ReturnValue::ServiceNotFound;
		}

		if (!ChangeServiceConfig(schService, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE,
			newPath, NULL, NULL, NULL, NULL, NULL, NULL)) {
			logIt(String::Format("ChangeServiceConfig failed: {0}", GetLastError()));
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
			goto TRYREG;
			return ReturnValue::PathNotFound;
		}

		logIt("Service path changed successfully.");
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return ReturnValue::Success;

	TRYREG:
		try {
			RegistryKey^ key = Registry::LocalMachine->OpenSubKey(String::Format("SYSTEM\\CurrentControlSet\\Services\\{0}", svcName), true);
			if (key != nullptr) {
				key->SetValue("ImagePath", obj->ToString());
				key->Close();
			}
			ServiceController^ sc = gcnew ServiceController(svcName);
			sc->Stop();
			sc->WaitForStatus(ServiceControllerStatus::Stopped);
			sc->Start();
			sc->WaitForStatus(ServiceControllerStatus::Running);
			return ReturnValue::Success;
		}
		catch (...) {

		}
		return ReturnValue::InvalidServiceControl;
		//String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		//ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		//try
		//{
		//	ManagementBaseObject^ inParams = service->GetMethodParameters("Change");
		//	inParams[sfield] = obj->ToString();
		//	try
		//	{
		//		ManagementBaseObject^ outParams = service->InvokeMethod("Change", inParams, nullptr);

		//		return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
		//	}
		//	catch (Exception^ ex)
		//	{
		//		logIt(ex->ToString());
		//		return ReturnValue::InvalidServiceControl;
		//	}
		//}
		//catch (Exception^ ex)
		//{
		//	logIt(ex->ToString());
		//	//if (ex->Message->ToLower()->Trim() == "not found" || ex->GetHashCode() == 41149443)
		//	return ReturnValue::ServiceNotFound;
		//}
	}


	ReturnValue StopService(String^ svcName)
	{
		logIt(String::Format("StopService  {0}", svcName));
		try {
			ServiceController^ sc = gcnew ServiceController(svcName);
			if (sc->Status == ServiceControllerStatus::Running)
			{
				sc->Stop();
				sc->WaitForStatus(ServiceControllerStatus::Stopped);
			}
		}
		catch (...) {
			return ReturnValue::ServiceRequestTimeout;
		}
		char szCmds[MAX_PATH] = { 0 };

		sprintf_s(szCmds, "net stop \"%s\"", (char*)(void*)Marshal::StringToHGlobalAnsi(svcName));
		system(szCmds);
		return ReturnValue::Success;
		//String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		//ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		//try
		//{
		//	ManagementBaseObject^ outParams = service->InvokeMethod("StopService", nullptr, nullptr);

		//	return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
		//}
		//catch (Exception^ ex)
		//{
		//	logIt(ex->ToString());
		//	//if (ex->Message->ToLower()->Trim() == "not found" || ex->GetHashCode() == 41149443)
		//		return ReturnValue::ServiceNotFound;
		//}
	}
	
	//not used
	ReturnValue ResumeService(String^ svcName)
	{
		logIt(String::Format("ResumeService  {0}", svcName));
		String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		try
		{
			ManagementBaseObject^ outParams = service->InvokeMethod("ResumeService", nullptr, nullptr);

			return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
		}
		catch (Exception^ ex)
		{
			logIt(ex->ToString());
			//if (ex.Message.ToLower().Trim() == "not found" || ex.GetHashCode() == 41149443)
			return ReturnValue::ServiceNotFound;

		}
	}
	//not used
	ReturnValue PauseService(String^ svcName)
	{
		logIt(String::Format("PauseService  {0}", svcName));
		String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		try
		{
			ManagementBaseObject^ outParams = service->InvokeMethod("PauseService", nullptr, nullptr);

			return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
		}
		catch (Exception^ ex)
		{
			logIt(ex->ToString());
			return ReturnValue::ServiceNotFound;
		}
	}
	//not used
	ReturnValue ChangeStartMode(String^ svcName, StartMode startMode)
	{
		logIt(String::Format("ChangeStartMode  {0}", svcName));
		String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		{
			ManagementBaseObject^ inParams = service->GetMethodParameters("ChangeStartMode");
			inParams["StartMode"] = startMode.ToString();
			try
			{
				ManagementBaseObject^ outParams = service->InvokeMethod("ChangeStartMode", inParams, nullptr);

				return (ReturnValue)Enum::Parse(ReturnValue::typeid, outParams["ReturnValue"]->ToString());
			}
			catch (Exception^ ex)
			{
				logIt(ex->ToString());
			}
		}
		return ReturnValue::UnknownFailure;
	}

	bool IsServiceInstalled(String^ svcName)
	{
		logIt(String::Format("IsServiceInstalled  {0}", svcName));
		try {
			ServiceController^ sc = gcnew ServiceController(svcName);
			ServiceControllerStatus status = sc->Status;
			logIt(String::Format("IsServiceInstalled  {0} status {1}", svcName, status));
			return true;
		}
		catch (...) {
			return false;
		}
		/*String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		try
		{
			ManagementBaseObject^ outParams = service->InvokeMethod("InterrogateService", nullptr, nullptr);

			return true;
		}
		catch (Exception^ ex)
		{
			if (ex->Message->ToLower()->Trim() == "not found" || ex->GetHashCode() == 41149443)
				System::Console::WriteLine("not found");
			return false;

		}*/
	}

	ServiceControllerStatus GetServiceState(String^ svcName)
	{
		logIt(String::Format("GetServiceState  {0}", svcName));
		try {
			ServiceController^ sc = gcnew ServiceController(svcName);
			ServiceControllerStatus status = sc->Status;
			logIt(String::Format("IsServiceInstalled  {0} status {1}", svcName, status));
			return status;
		}
		catch (...) {
			return ServiceControllerStatus::Stopped;
		}
		/*ServiceState toReturn = ServiceState::Stopped;
		String^ _state = String::Empty;

		String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		{
			try
			{
				_state = service->Properties["State"]->Value->ToString()->Trim();
				logIt(String::Format("GetServiceState:{0}==>{1}", svcName, _state));
				if (String::Compare("Running", _state, true) == 0)
					toReturn = ServiceState::Running;
				else if (String::Compare("Stopped", _state, true) == 0)
					toReturn = ServiceState::Stopped;
				else if (String::Compare("Paused", _state, true) == 0)
					toReturn = ServiceState::Paused;
				else if (String::Compare("Start Pending", _state, true) == 0)
					toReturn = ServiceState::StartPending;
				else if (String::Compare("Stop Pending", _state, true) == 0)
					toReturn = ServiceState::StopPending;
				else if (String::Compare("Continue Pending", _state, true) == 0)
					toReturn = ServiceState::ContinuePending;
				else if (String::Compare("Pause Pending", _state, true) == 0)
					toReturn = ServiceState::PausePending;
				else 
				{}
			}
			catch (Exception^ ex)
			{
				throw ex;
			}
		}*/
		return ServiceControllerStatus::Stopped;
	}

	bool CanStop(String^ svcName)
	{
		logIt(String::Format("CanStop  {0}", svcName));
		try {
			ServiceController^ sc = gcnew ServiceController(svcName);
			return sc->CanStop;
		}
		catch (...) {
			return false;
		}
		/*String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		{
			try
			{
				logIt(String::Format("CanStop:{0}==>{1}", svcName, service->Properties["AcceptStop"]->Value->ToString()));
				return bool::Parse(service->Properties["AcceptStop"]->Value->ToString());
			}
			catch (Exception^ ex)
			{
				logIt(ex->ToString());
				return false;
			}
		}*/
	}

	bool CanPauseAndContinue(String^ svcName)
	{
		logIt(String::Format("CanPauseAndContinue  {0}", svcName));
		try {
			ServiceController^ sc = gcnew ServiceController(svcName);
			return sc->CanPauseAndContinue;
		}
		catch (...) {
			return false;
		}
		/*String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		{
			try
			{
				return bool::Parse(service->Properties["AcceptPause"]->Value->ToString());
			}
			catch (Exception^ ex)
			{
				logIt(ex->ToString());
				return false;
			}
		}*/
	}

	int GetProcessId(String^ svcName)
	{
		logIt(String::Format("GetProcessId  {0}", svcName));
		String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		{
			try
			{
				logIt(String::Format("GetProcessId:{0}==>{1}", svcName, service->Properties["ProcessId"]->Value->ToString()));
				return int::Parse(service->Properties["ProcessId"]->Value->ToString());
			}
			catch (Exception^ ex)
			{
				logIt(ex->ToString());
				return 0;
			}
		}
	}

	String^ GetPath(String^ svcName)
	{
		logIt(String::Format("GetPath  {0}", svcName));
		String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		try
		{
			logIt(String::Format("GetPath:{0}==>{1}", svcName, service->Properties["PathName"]->Value->ToString()));
			return service->Properties["PathName"]->Value->ToString();
		}
		catch (Exception^ ex)
		{
			logIt(ex->ToString());
			return String::Empty;
		}
	}


	bool ShowProperties(String^ svcName)
	{
		String^ objPath = String::Format("Win32_Service.Name='{0}'", svcName);
		ManagementObject^ service = gcnew ManagementObject(gcnew ManagementPath(objPath));
		{
			try
			{
				for each(PropertyData^ a in service->SystemProperties)
					System::Console::WriteLine(propData2String(a));
				for each(PropertyData^ a in service->Properties)
					System::Console::WriteLine(propData2String(a));
				if (service->Properties["State"]->Value->ToString()->Trim() == "Running")
					return true;
			}
			catch (Exception^ ex)
			{
				Console::WriteLine(ex->ToString());
				return false;
			}
		}
		return false;
	}

	String^ propData2String(PropertyData^ pd)
	{
		
		String^ toReturn = "Name: " + pd->Name + Environment::NewLine;
		toReturn += "IsArray: " + pd->IsArray.ToString() + Environment::NewLine;
		toReturn += "IsLocal: " + pd->IsLocal.ToString() + Environment::NewLine;
		toReturn += "Origin: " + pd->Origin + Environment::NewLine;
		toReturn += "CIMType: " + pd->Type.ToString() + Environment::NewLine;
		if (pd->Value != nullptr)
			toReturn += "Value: " + pd->Value->ToString() + Environment::NewLine;
		else
			toReturn += "Value is nullptr" + Environment::NewLine;
		int i = 0;
		for each(QualifierData^ qd in pd->Qualifiers)
		{
			toReturn += "\tQualifier[" + i.ToString() + "]IsAmended: " + qd->IsAmended.ToString() + Environment::NewLine;
			toReturn += "\tQualifier[" + i.ToString() + "]IsLocal: " + qd->IsLocal.ToString() + Environment::NewLine;
			toReturn += "\tQualifier[" + i.ToString() + "]IsOverridable: " + qd->IsOverridable.ToString() + Environment::NewLine;
			toReturn += "\tQualifier[" + i.ToString() + "]Name: " + qd->Name + Environment::NewLine;
			toReturn += "\tQualifier[" + i.ToString() + "]PropagatesToInstance: " + qd->PropagatesToInstance.ToString() + Environment::NewLine;
			toReturn += "\tQualifier[" + i.ToString() + "]PropagatesToSubclass: " + qd->PropagatesToSubclass.ToString() + Environment::NewLine;
			if (qd->Value != nullptr)
				toReturn += "\tQualifier[" + i.ToString() + "]Value: " + qd->Value->ToString() + Environment::NewLine;
			else
				toReturn += "\tQualifier[" + i.ToString() + "]Value is nullptr" + Environment::NewLine;
			i++;
		}
		return toReturn;
	}
}
