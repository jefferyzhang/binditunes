// bindItuns.cpp : main project file.

#include "stdafx.h"
#include <Windows.h>

#include "iDevServicePatch.h"
#include "ServicesManage.h"
#include "iTunesRegistry.h"
#include "Globals.h"

using namespace System;
using namespace System::Collections;
using namespace System::Collections::Specialized;
using namespace System::Configuration::Install;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace Microsoft::Win32;
using namespace System::Globalization;
using namespace System::Security::Principal;
using namespace System::Xml;


BOOL bForceCopy = false;

#define iPodService	"iPod Service"
#define BonjourService	"Bonjour Service"
#define AppleMobileDevice "Apple Mobile Device"
#define AppleMobileDevice12 "Apple Mobile Device Service"

void logIt(System::String^ msg)
{
	System::String^ ss = System::String::Format(L"[BindItunes]: {0}", msg);
	System::Diagnostics::Trace::WriteLine(ss);
	Console::WriteLine(ss);
}


void StopiPodService()
{
	if (Installation::IsServiceInstalled(iPodService))
	{
		if (Installation::CanStop(iPodService))
		{
			Installation::StopService(iPodService);
		}
	}
}

void StartiPodService()
{
	if (Installation::IsServiceInstalled(iPodService))
	{
		Installation::StartServiceFD(iPodService);
	}
}

void ReplaceBonjorService(String^ targetpath, String^ mDNSResponder)
{
	if (Installation::CanStop(BonjourService))
	{
		Installation::StopService(BonjourService);
	}
	File::Copy(targetpath, mDNSResponder);
	Installation::StartServiceFD(BonjourService);
}

void StopRestartAppleMobileDevice(String^ sPath)
{
	if (Installation::IsServiceInstalled(AppleMobileDevice12))
	{
		String^ sP = Installation::GetPath(AppleMobileDevice12);
		logIt(String::Format("Apple Mobile Device Path:{0}", sP));
		if (String::Compare(sPath, sP, true) == 0)
		{
			Installation::StartServiceFD(AppleMobileDevice12);
			return;
		}
		int ntrycount = 5;
		if (File::Exists(sPath))
		{
			do{
				Installation::ReturnValue a = Installation::ChangeService(AppleMobileDevice12, "PathName", sPath);
				logIt(a.ToString());
				if (a == Installation::ReturnValue::Success) break;
				Sleep(1000);
			} while (ntrycount-- > 0);
		}
		else
		{
			Installation::StopService(AppleMobileDevice12);
		}
	}
	//Apple Mobile Device
	else if (Installation::IsServiceInstalled(AppleMobileDevice))
	{
		String^ sP = Installation::GetPath(AppleMobileDevice);
		logIt(String::Format("Apple Mobile Device Path:{0}", sP));
		if (String::Compare(sPath, sP, true) == 0)
		{
			Installation::StartServiceFD(AppleMobileDevice);
			return;
		}
		int ntrycount = 5;
		if (File::Exists(sPath))
		{
			do {
				Installation::ReturnValue a = Installation::ChangeService(AppleMobileDevice, "PathName", sPath);
				logIt(a.ToString());
				if (a == Installation::ReturnValue::Success) break;
				Sleep(1000);
			} while (ntrycount-- > 0);
		}
		else
		{
			Installation::StopService(AppleMobileDevice);
			Installation::UninstallService(AppleMobileDevice);
		}

	}
}


#define MYFDPATH	"PST\\Apple"
#define MYCOPYPATH	"Common Files\\Apple"
//now set myfdpath is APSTHOME\\..\\CommomFiles\\Apple

bool IsUserAdministrator()
{
	//bool value to hold our return value
	bool isAdmin;
	try
	{
		//get the currently logged in user
		WindowsIdentity^ user = WindowsIdentity::GetCurrent();
		WindowsPrincipal^ principal = gcnew WindowsPrincipal(user);
		isAdmin = principal->IsInRole(WindowsBuiltInRole::Administrator);
	}
	catch (UnauthorizedAccessException^ ex)
	{
		logIt("current user is not administrator " + ex->ToString());
		isAdmin = false;
	}
	catch (Exception^ ex)
	{
		logIt("current user is not administrator " + ex->ToString());
		isAdmin = false;
	}
	return isAdmin;
}

String^ createXML(String^ exePath, String^ param)
{
	String^ savePath = String::Empty;
	try
	{
		XmlDocument^ fdxml = gcnew XmlDocument();
		savePath = System::IO::Path::GetTempFileName();
		XmlNode^ rootNode = fdxml->CreateNode(XmlNodeType::Element, "runexe", nullptr);
		fdxml->AppendChild(rootNode);
		XmlNode^ exeNode = fdxml->CreateNode(XmlNodeType::Element, "exepath", nullptr);
		exeNode->InnerText = exePath;
		rootNode->AppendChild(exeNode);
		XmlNode^ paramNode = fdxml->CreateNode(XmlNodeType::Element, "parameter", nullptr);
		paramNode->InnerText = param;
		rootNode->AppendChild(paramNode);
		logIt("BindiTunes will save runxml to " + savePath);
		logIt(fdxml->OuterXml);
		fdxml->Save(savePath);
	}
	catch (Exception^ ex)
	{
		logIt("createXML exception " + ex->ToString());
	}
	return savePath;
}

int call_RMDU(String^ exeFilename, String^ args)
{
	int ret = -1;
	String^ xmlPath = createXML(exeFilename, args);
	try{
		String^ runRMDU = Path::Combine(Environment::GetEnvironmentVariable("APSTHOME"), "FDU.exe");
		if (File::Exists(xmlPath) && File::Exists(runRMDU))
		{
			Process^ myProcess = gcnew Process();
			//ProcessStartInfo^ startInfo = gcnew ProcessStartInfo();
			myProcess->StartInfo->FileName = runRMDU;
			myProcess->StartInfo->Arguments = String::Format("\"{0}\"", xmlPath);
			myProcess->StartInfo->WindowStyle = ProcessWindowStyle::Hidden;
			myProcess->StartInfo->CreateNoWindow = true;
			myProcess->StartInfo->UseShellExecute = false;
			myProcess->Start();
			myProcess->WaitForExit(5000);
			ret = myProcess->ExitCode;
			
		}
	}
	catch(Exception^ ex)
	{
		logIt(ex->ToString());
		ret = -1;
	}
	finally
	{
		File::Delete(xmlPath);
	}
	return ret;

}
void OutputHandler(Object^ /*sendingProcess*/,
	DataReceivedEventArgs^ outLine)
{
	// Collect the sort command output. 
	if (!String::IsNullOrEmpty(outLine->Data))
	{
		logIt(outLine->Data);
		Globals::exeoutlist->Add(outLine->Data);
	}
}
int RunExe(String^ exeFilename, String^ args)
{
	int ret = -1;
	logIt(String::Format("Command line {0}", args));
	try{
		if (File::Exists(exeFilename))
		{
			Process^ myProcess = gcnew Process();
			//ProcessStartInfo^ startInfo = gcnew ProcessStartInfo();
			myProcess->StartInfo->FileName = exeFilename;
			myProcess->StartInfo->Arguments = args;
			myProcess->StartInfo->WindowStyle = ProcessWindowStyle::Hidden;
			myProcess->StartInfo->CreateNoWindow = true;
			myProcess->StartInfo->UseShellExecute = false;
			myProcess->StartInfo->RedirectStandardOutput = true;
			myProcess->OutputDataReceived += gcnew DataReceivedEventHandler(OutputHandler);
			myProcess->Start();
			myProcess->BeginOutputReadLine();
			myProcess->WaitForExit();
			ret = myProcess->ExitCode;

		}
	}
	catch (Exception^ ex)
	{
		logIt(ex->ToString());
		ret = -1;
	}
	return ret;

}

/// <summary>
/// Compare versions of form "1,2,3,4" or "1.2.3.4". Throws FormatException
/// in case of invalid version.
/// </summary>
/// <param name="strA">the first version</param>
/// <param name="strB">the second version</param>
/// <returns>less than zero if strA is less than strB, equal to zero if
/// strA equals strB, and greater than zero if strA is greater than strB</returns>
int CompareVersions(String^ strA, String^ strB)
{
	Version^ vA = gcnew Version(strA->Replace(",", "."));
	Version^ vB = gcnew Version(strB->Replace(",", "."));

	return vA->CompareTo(vB);
}

BOOL MoveServiceFile(String^ srcFile, String^ dstFile)
{
	BOOL bret = false;
	logIt(String::Format("MoveServiceFile ++ {0}=={1}", srcFile, dstFile));
	String^ soldfilename = Path::Combine(dstFile, "AppleMobileDeviceService.exe");
	String^ snewFileName = Path::Combine(srcFile, "phonedll\\PST_APE_UNIVERSAL_USB_FD\\resource\\AppleMobileDeviceService.exe");
	if (!File::Exists(snewFileName)) {
		snewFileName = Path::Combine(srcFile, "iosTools\\AppleMobileDeviceService.exe");
	}
	if (File::Exists(soldfilename) && File::Exists(snewFileName))
	{
		FileVersionInfo^ oldv = FileVersionInfo::GetVersionInfo(soldfilename);
		FileVersionInfo^ oldnew = FileVersionInfo::GetVersionInfo(snewFileName);
		if (CompareVersions(oldv->FileVersion, oldnew->FileVersion) != 0)
		{
			logIt("version is defferent.");
			try
			{
				File::Copy(snewFileName, soldfilename, TRUE);
			}
			catch (Exception^ ex){
				logIt(ex->ToString());
			}
		}
		else
		{
			logIt("Not need copy Service File.");
			bret = true;
		}
	}
	else if (!File::Exists(soldfilename) && File::Exists(snewFileName))
	{
		logIt("server path not include apple service.");
		try
		{
			File::Copy(snewFileName, soldfilename, TRUE);
		}
		catch (Exception^ ex){
			logIt(ex->ToString());
		}
	}
	else
	{
		logIt("not found apple service ! please reinstall or fixed it.");
	}

	return bret;
}

int nExtraSuccess = ERROR_SUCCESS;
BOOL CopyiTunesRunTime(String^ apsthomepath, String^ sAppleComFiles)
{
	logIt(String::Format("CopyiTunesRunTime++ {0}=={1}", apsthomepath, sAppleComFiles));
	BOOL bRet = false;
	DirectoryInfo^ dinfo = Directory::GetParent(apsthomepath);
	logIt(dinfo->ToString());
	//String^ sAppleComFiles = Path::Combine(dinfo->ToString(), "Common Files\\Apple");
	String^ sFileupdate = Path::Combine(sAppleComFiles, "Mobile Device Support\\deployzip.txt");
	String^ sAppleRunTimeZip = Path::Combine(apsthomepath, "PST\\Apple\\AppleRunTime.zip");
	DateTime lastModified = File::GetLastWriteTime(sAppleRunTimeZip);
	if (!Directory::Exists(sAppleComFiles))
	{
		Directory::CreateDirectory(sAppleComFiles);
		//CopyFile zip file to this 
		Globals::ClearOutList();
		String^ zip7exe = Path::Combine(apsthomepath, "7z.exe");
		if (File::Exists(zip7exe) && File::Exists(sAppleRunTimeZip))
		{
			int pExitCode = RunExe(zip7exe, String::Format("x \"{0}\" -o\"{1}\" -aoa -r -y", sAppleRunTimeZip, sAppleComFiles));
			Sleep(500);
			if (pExitCode == 0)
			{
				if (Globals::FindStr("Everything is Ok")) {
					bRet = true;
					logIt("install apple runtime successful.");
					File::WriteAllText(sFileupdate, lastModified.ToString());
				}
				else {
					logIt("unzip apple runtime failed.");
					nExtraSuccess = ERROR_EXTRANEOUS_INFORMATION;
				}
			}
			else
			{
				logIt("install apple runtime failed.");
				nExtraSuccess = ERROR_EXTRANEOUS_INFORMATION;
			}
		}
		else
		{
			logIt(String::Format("7z.exe or apple runtime file is not exist."));
			nExtraSuccess = ERROR_FILE_EXISTS;
		}

	}
	else
	{
		BOOL bCopy = true;
		if (File::Exists(sFileupdate)) {
			String^ datest = File::ReadAllText(sFileupdate)->Trim();
			if (String::Compare(lastModified.ToString(), datest, true) == 0) {
				bCopy = false;
			}
		}
		if (bForceCopy||bCopy)
		{
			Globals::ClearOutList();
			String^ zip7exe = Path::Combine(apsthomepath, "7z.exe");
			int pExitCode = RunExe(zip7exe, String::Format("x \"{0}\" -o\"{1}\" -aoa -r -y", sAppleRunTimeZip, sAppleComFiles));
			Sleep(500);
			if (pExitCode == 0)
			{
				if (Globals::FindStr("Everything is Ok")) {
					bRet = true;
					logIt("install apple1 runtime successful.");
					File::WriteAllText(sFileupdate, lastModified.ToString());
				}
				else {
					logIt("unzip apple1 runtime failed.");
					nExtraSuccess = ERROR_EXTRANEOUS_INFORMATION;
				}
			}
			else
			{
				logIt("install apple1 runtime failed.");
				nExtraSuccess = ERROR_EXTRANEOUS_INFORMATION;
			}
		}
		else {
			bRet = true;
		}
	}
	
	return bRet;
}

BOOL CopyiTunesX32Dll(String^ apsthomepath) {
	BOOL bRet = false;
	logIt(String::Format("CopyiTunesX32Dll++ {0}", apsthomepath));
	String^ sApplex32RunTimeZip = Path::Combine(apsthomepath, "PST\\Apple\\iTunesDLL.zip");
	if (!File::Exists(sApplex32RunTimeZip)) return false;
	DateTime lastModified = File::GetLastWriteTime(sApplex32RunTimeZip);
	String^ swritetimefile = Path::Combine(apsthomepath, "iTunesDLL\\updatedate.txt");
	if (File::Exists(swritetimefile)) {
		String^ datest = File::ReadAllText(swritetimefile)->Trim();
		logIt(String::Format("zip: {0}   updatelog: {1}", lastModified.ToString(), datest));
		try {
			if (String::Compare(lastModified.ToString(), datest, true) == 0) {
				return true;
			}

			if (DateTime::Equals(lastModified, Convert::ToDateTime(datest))) {
				return true;
			}
		}
		catch (...) {

		}
	}
	Globals::ClearOutList();
	String^ zip7exe = Path::Combine(apsthomepath, "7z.exe");
	int pExitCode = RunExe(zip7exe, String::Format("x \"{0}\" -o\"{1}\" -aoa -r -y", sApplex32RunTimeZip, apsthomepath));
	Sleep(500);
	if (pExitCode == 0)
	{
		if (Globals::FindStr("Everything is Ok")) {
			bRet = true;
			logIt("install apple x86 runtime successful.");
			File::WriteAllText(swritetimefile, lastModified.ToString());
		}
		else {
			logIt("unzip apple x86 runtime failed.");
		}
	}
	else
	{
		logIt("install apple x86 runtime failed.");
	}
	return bRet;
}

BOOL DeleteOldFiles(String^ sFDAppleMobileDeviceSupport) {
	if (!File::Exists(Path::Combine(sFDAppleMobileDeviceSupport, "iTunes.txt"))) {
		// Delete all files
		for each(String^ file in Directory::GetFiles(sFDAppleMobileDeviceSupport))
		{
			try {
				File::Delete(file);
			}catch(...){}
		}

		// Delete all subdirectories and their contents
		for each(String^ subfolder in Directory::GetDirectories(sFDAppleMobileDeviceSupport))
		{
			try {
				Directory::Delete(subfolder, true); // 'true' deletes subdirectories and files recursively
			}catch(...){}
		}
		File::WriteAllText(Path::Combine(sFDAppleMobileDeviceSupport, "iTunes.txt"), DateTime::Now.ToString());
		return TRUE;
	}
	return FALSE;
}

int main(array<System::String ^> ^args)
{
	logIt(String::Format("{0} start: ++ version: {1}",
		Path::GetFileName(Process::GetCurrentProcess()->MainModule->FileName),
		Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion));

	StringBuilder^ sb = gcnew StringBuilder();
	for each (System::String ^ ss in args)
	{
		logIt(ss);
		sb->Append(String::Format("\"{0}\" ", ss));
	}	
	InstallContext^ myInstallContext = gcnew InstallContext(nullptr,args);
	if (myInstallContext->IsParameterTrue("debug"))
	{
		logIt("Wait any key to dubug, Please attach...");
		System::Console::ReadKey();
	}
	
	if (myInstallContext->IsParameterTrue("forcecopy")) {
		bForceCopy = true;
	}

	if (!IsUserAdministrator())
	{
		call_RMDU(Process::GetCurrentProcess()->MainModule->FileName, sb->ToString());
		return ERROR_EA_ACCESS_DENIED;
	}

	System::Threading::Mutex^ g_mutex = nullptr;

	bool bExit = false;
	try
	{
		System::Threading::Mutex::OpenExisting("Global\\bind_itunes_jeffery_Mutex");
		logIt("Only one instance of this application is allowed!");
		bExit = true;
	}
	catch (System::Threading::WaitHandleCannotBeOpenedException^ )
	{
		// mutex doesn't exist
		g_mutex = gcnew System::Threading::Mutex(true, "Global\\bind_itunes_jeffery_Mutex");
	}
	catch (Exception^)
	{
		logIt("Create mutex error.");
		bExit = true;
	}

	if (bExit)
	{
		if (g_mutex != nullptr)
		{
			g_mutex->ReleaseMutex();
			g_mutex = nullptr;
		}	
		return ERROR_SERVICE_ALREADY_RUNNING;
	}

	Installation::StartServiceFD("Winmgmt");

	String^ sAPath = Path::Combine(Environment::GetFolderPath(Environment::SpecialFolder::CommonProgramFiles), "Apple\\Mobile Device Support");
	sAPath = Path::Combine(sAPath, "AppleMobileDeviceService.exe");
	logIt(sAPath);
	if (File::Exists(sAPath))
	{ 
		logIt("iTunes Installed!");
	}
	else
	{
		logIt("iTunes not Installed!");
	}

	//Installation::ReturnValue a = Installation::ChangeService("Apple Mobile Device", "PathName", sAPath);
	//logIt(a.ToString());

	String^ sApsthomePath = Environment::GetEnvironmentVariable("APSTHOME");
	CopyiTunesX32Dll(sApsthomePath);

	sApsthomePath = sApsthomePath->TrimEnd(Path::DirectorySeparatorChar);
	DirectoryInfo^ dinfo = Directory::GetParent(sApsthomePath);
	String^ sFDHomePath = dinfo->ToString();
	logIt(String::Format("futuredial home {0}", sFDHomePath));

	if (myInstallContext->IsParameterTrue("debug"))
		sApsthomePath = "D:\\temp\\APSTHOME";
	if (!Directory::Exists(sApsthomePath))
	{
		logIt("can not find APSTHOME PATH. exit.");
		return ERROR_BAD_ENVIRONMENT;
	}

	String^ sApsthomeMYFDPath = Path::Combine(sApsthomePath, MYFDPATH);
	if (myInstallContext->IsParameterTrue("copyver"))
	{
		sApsthomeMYFDPath = Path::Combine(sFDHomePath, MYCOPYPATH);
		logIt(String::Format("futuredial copy {0}", sApsthomeMYFDPath));
	}
	if (myInstallContext->Parameters->ContainsKey("path"))
	{
		sApsthomeMYFDPath = Environment::ExpandEnvironmentVariables(myInstallContext->Parameters["path"]);
	}

	//String^ sFDBonjourPath;
	String^ sFDAppleMobileDeviceSupport;
	//String^ sFDAppleApplicationSupport;
	if (Directory::Exists(sApsthomePath))
	{
		if (!Directory::Exists(sApsthomeMYFDPath)) Directory::CreateDirectory(sApsthomeMYFDPath);
		logIt("Check iTunes install status");
		//sFDBonjourPath = Path::Combine(sApsthomeMYFDPath, "Bonjour");
		sFDAppleMobileDeviceSupport = Path::Combine(sApsthomeMYFDPath, "Mobile Device Support");
		//sFDAppleApplicationSupport = Path::Combine(sApsthomeMYFDPath, "Apple Application Support");
		/*if (!Directory::Exists(sFDBonjourPath))
		{
			logIt("FD No Bonjour Path. Use System installed."); 
		}*/
		//if (!Directory::Exists(sFDAppleMobileDeviceSupport) ||
		//	!Directory::Exists(sFDAppleApplicationSupport)){
		//	logIt("Must Apple runtime environment.");
		//	return ERROR_ENVVAR_NOT_FOUND;
		//}
	}
	//G:\ProgramData\Futuredial\CMC\PST\Apple\Common

	if (myInstallContext->Parameters->ContainsKey("install"))
	{
		//String^ sFDBonjourFile = Path::Combine(sFDBonjourPath, "mDNSResponder.exe");
		String^ sysinstallBonjour = Path::Combine(Environment::GetFolderPath(Environment::SpecialFolder::ProgramFiles), "Bonjour", "mDNSResponder.exe");
		String^ sysinstallBonjourx86 = Path::Combine(Environment::GetFolderPath(Environment::SpecialFolder::ProgramFilesX86), "Bonjour", "mDNSResponder.exe");
		bool isinstallbonjour = Installation::IsServiceInstalled(BonjourService);
		if (isinstallbonjour || File::Exists(sysinstallBonjour) || File::Exists(sysinstallBonjourx86))
		{
			if (!isinstallbonjour) {
				logIt("Bonjour service is not exist. but file found.");
				String^ sFDBonjourFile = File::Exists(sysinstallBonjour) ? sysinstallBonjour : sysinstallBonjourx86;
				Installation::InstallService(BonjourService, BonjourService, sFDBonjourFile,
					Installation::ServiceType::OwnProcess, Installation::OnError::UserIsNotNotified, Installation::StartMode::Automatic,
					false, nullptr, nullptr, nullptr, nullptr, nullptr);
				Installation::StartServiceFD(BonjourService);
			}
		}
		else
		{
			// system no install apple runtime
			logIt("System no install Bonjour. please install Bonjour.");
			return ERROR_BAD_ENVIRONMENT;
		}
		//modify registry
		StopiPodService();

		if (Installation::IsServiceInstalled(AppleMobileDevice12))
		{
			Installation::StopService(AppleMobileDevice12);
		}

		BOOL bCopyRuntime = CopyiTunesRunTime(sApsthomePath, sApsthomeMYFDPath);

		BOOL bAMDInsted = FALSE , bAMDInsted12 = FALSE;
		if (Installation::IsServiceInstalled(AppleMobileDevice))
		{
			Installation::StopService(AppleMobileDevice);
		}
		if (Installation::IsServiceInstalled(AppleMobileDevice12))
		{
			Installation::StopService(AppleMobileDevice12);
		}
		//MoveServiceFile(sApsthomePath, sFDAppleMobileDeviceSupport);
		if (DeleteOldFiles(sFDAppleMobileDeviceSupport)) {
			bForceCopy = true;
		}

		ApplePrepareDriver();

		if (GetFirstIdeviceInstance() == ERROR_SUCCESS)
		{
			DEDriverUSBDevice();
		}

		if (!bCopyRuntime || bForceCopy)
			CopyiTunesRunTime(sApsthomePath, sApsthomeMYFDPath);
		//else
		//	MoveServiceFile(sApsthomePath, sFDAppleMobileDeviceSupport);

		if (/*(bAMDInsted = Installation::IsServiceInstalled(AppleMobileDevice)) ||*/
			(bAMDInsted12 = Installation::IsServiceInstalled(AppleMobileDevice12)))
		{
			if (bAMDInsted)
			{
				iTunesRegistry::CreateiTunesRegInfo(sApsthomeMYFDPath); // set fd path
				Installation::ReturnValue a = Installation::ChangeService(AppleMobileDevice, "PathName", Path::Combine(sFDAppleMobileDeviceSupport, "AppleMobileDeviceService.exe"));
				if (a != Installation::ReturnValue::Success)
				{
					logIt(a.ToString());
					StopRestartAppleMobileDevice(Path::Combine(sFDAppleMobileDeviceSupport, "AppleMobileDeviceService.exe"));
				}
				else
				{
					Installation::StopService(AppleMobileDevice);
					if (Installation::GetServiceState(AppleMobileDevice) == ServiceControllerStatus::Stopped)
					{
						a = Installation::StartServiceFD(AppleMobileDevice);
						int i = 5;
						while (a != Installation::ReturnValue::Success && i-- > 0)
						{
							Sleep(500);
							a = Installation::StartServiceFD(AppleMobileDevice);
						}
						if (a != Installation::ReturnValue::Success)
						{
							logIt(String::Format("start service failed: {0}", a.ToString()));
						}
					}
				}
			}
			else if (bAMDInsted12)
			{
				iTunesRegistry::CreateiTunesRegInfo(sApsthomeMYFDPath); // set fd path
				Installation::ReturnValue a = Installation::ChangeService(AppleMobileDevice12, "PathName", Path::Combine(sFDAppleMobileDeviceSupport, "AppleMobileDeviceService.exe"));
				if (a != Installation::ReturnValue::Success)
				{
					logIt(a.ToString());
					StopRestartAppleMobileDevice(Path::Combine(sFDAppleMobileDeviceSupport, "AppleMobileDeviceService.exe"));
				}
				else
				{
					Installation::StopService(AppleMobileDevice12);
					if (Installation::GetServiceState(AppleMobileDevice12) == ServiceControllerStatus::Stopped)
					{
						a = Installation::StartServiceFD(AppleMobileDevice12);
						int i = 5;
						while (a != Installation::ReturnValue::Success && i-- > 0)
						{
							Sleep(500);
							a = Installation::StartServiceFD(AppleMobileDevice);
						}
						if (a != Installation::ReturnValue::Success)
						{
							logIt(String::Format("start service failed: {0}", a.ToString()));
						}
					}

				}
			}
		}
		else
		{
			iTunesRegistry::CreateiTunesRegInfo(sApsthomeMYFDPath); // set fd path
			Installation::InstallService(AppleMobileDevice12, AppleMobileDevice12, Path::Combine(sFDAppleMobileDeviceSupport, "AppleMobileDeviceService.exe"),
				Installation::ServiceType::OwnProcess, Installation::OnError::UserIsNotNotified, Installation::StartMode::Automatic,
				false, nullptr, nullptr, nullptr, nullptr, nullptr);

			Installation::ReturnValue a = Installation::StartServiceFD(AppleMobileDevice12);
			int i = 5;
			while (a != Installation::ReturnValue::Success && i-- > 0)
			{
				Sleep(500);
				a = Installation::StartServiceFD(AppleMobileDevice12);
			}
		}

		DEDriverUSBDevice(FALSE);
		//install APSDeamon Server?? command line:RegServer UnregServer
	}
	else if (myInstallContext->Parameters->ContainsKey("uninstall"))
	{
		StartiPodService();
		String^ sAppleCommonPath = Path::Combine(Environment::GetFolderPath(Environment::SpecialFolder::CommonProgramFiles), "Apple");
		if (Directory::Exists(sAppleCommonPath))
		{
			iTunesRegistry::CreateiTunesRegInfo(sAppleCommonPath); //restore itunes path
		}
		StopRestartAppleMobileDevice(Path::Combine(sAppleCommonPath,"Mobile Device Support\\AppleMobileDeviceService.exe"));

		if (Installation::IsServiceInstalled(AppleMobileDevice))
		{
			Installation::StopService(AppleMobileDevice);
		}
		if (Installation::IsServiceInstalled(AppleMobileDevice12))
		{
			Installation::StopService(AppleMobileDevice12);
		}

	}
	else if (myInstallContext->Parameters->ContainsKey("restart"))
	{
		try{
			if (Installation::IsServiceInstalled(AppleMobileDevice) && (Installation::GetServiceState(AppleMobileDevice) != ServiceControllerStatus::Running))
			{
				logIt("restart serivce " + AppleMobileDevice);
				Installation::StartServiceFD(AppleMobileDevice);
			}

		}
		catch (...)
		{

		}
		try
		{
			if (Installation::IsServiceInstalled(AppleMobileDevice12) && Installation::GetServiceState(AppleMobileDevice12) != ServiceControllerStatus::Running)
			{
				logIt("restart serivce " + AppleMobileDevice12);
				Installation::StartServiceFD(AppleMobileDevice12);
			}
		}
		catch (...)
		{

		}
	}
	if (g_mutex != nullptr)
	{
		g_mutex->ReleaseMutex();
		g_mutex = nullptr;
	}
	logIt(String::Format("binditunes exit code={0}", nExtraSuccess));
	return nExtraSuccess;
}
